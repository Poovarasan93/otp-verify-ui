import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider';
import { Checkbox } from 'react-native-paper';


const Page = ({ navigation }) => {
  const [checked, setChecked] = React.useState(false);

  const RenderDoneButton = () => {
    return (
      <View >
        <View style={styles.checkBox}>
          <Checkbox
            status={checked ? 'checked' : 'unchecked'}
            onPress={() => {
              setChecked(!checked);
            }}
            color={'green'}
          /></View>
        <Text style={styles.end}>

          <Text>I have read all the </Text>
          <Text style={styles.highlight}>Terms and Conditions</Text>
          <Text> and </Text>
          <Text style={styles.highlight}>Privacy Policy</Text>
          <Text>{'\n'}and Agree to the same</Text>
        </Text>


        <TouchableOpacity style={styles.button} >
          <Text style={styles.buttonText} onPress={() => navigation.navigate('Login')} >Get Started</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const RenderItem = ({ item }) => {
    return (
      <View style={styles.container}>
        <Text style={styles.introTitleStyle}>
          {item.title}
        </Text>
        <Image
          style={styles.introImageStyle}
          source={item.image} />
        <Text style={styles.introHead} >{item.heading}</Text>
        <Text style={styles.introTextStyle}>
          {item.text}
        </Text>

      </View>
    );
  };

  return (
    <AppIntroSlider
      data={slides}
      renderItem={RenderItem}
      renderDoneButton={RenderDoneButton}
    />
  );
};

export default Page;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',

  },
  titleStyle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',

  },
  introImageStyle: {
    width: 300,
    height: 250,
    alignSelf: 'center',
    marginTop: 20
  },
  introHead: {
    color: 'black',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 45
  },
  introTextStyle: {
    fontSize: 18,
    color: 'gray',
    textAlign: 'center',
    paddingVertical: 20,
    padding: 10,
    justifyContent: "space-evenly"
  },
  introTitleStyle: {
    fontSize: 28,
    color: 'black',
    paddingTop: 50,
    marginLeft: 15,
    fontWeight: 'bold'
  },
  button: {
    width: 200,
    height: 40,
    borderRadius: 5,
    backgroundColor: '#0095C6',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 60,
    alignSelf: 'center',
    bottom: 120
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold'
  },
  end: {
    bottom: 130,
    fontSize: 13,
    left: 10,
    margin: 5
  },
  highlight: {
    color: '#0095C6',
    textDecorationLine: 'underline',
  },
  checkBox: {
    bottom: 99,
    right: 18,

  }
});

const slides = [
  {
    key: 's1',
    text: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno Lorem Ipsum es  ',
    title: 'Welcome to Freerides',
    image: require('../asset/image1.png'),
    backgroundColor: 'white',
    heading: 'Create A Profile'
  },
  {
    key: 's2',
    title: 'Welcome to Freerides',
    text: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno Lorem Ipsum es',
    image: require('../asset/image3.png'),
    backgroundColor: 'white',
    heading: 'Find & Offer Comfortable Ride'
  },
  {
    key: 's3',
    title: 'Welcome to Freerides',
    text: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno Lorem Ipsum es  ',
    image: require('../asset/image2.png'),
    backgroundColor: 'white',
    heading: 'Join the Community',

  }
];