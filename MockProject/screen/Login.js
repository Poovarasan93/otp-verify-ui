import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import PhoneInput from "react-native-phone-number-input";



const Login = ({ navigation }) => {

  const [value, setValue] = React.useState('')

  return (
    <View style={styles.container}>
      <Text style={styles.text1} >Login</Text>
      <Text style={styles.text2}>Enter your Mobile Number for OTP</Text>
      <View style={styles.phone}>
        <PhoneInput style={styles.text3} placeholder="Enter Valid number" value={value} onChange={setValue} />
      </View>
      <TouchableOpacity
        style={styles.button}>
        <Text style={styles.input} onPress={() => navigation.navigate('Verify')} >Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    backgroundColor: 'white',
    flex: 1
  },
  image1: {
    height: 250,
    width: 250,
  },
  text1: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
    paddingTop: 20
  },
  text2: {
    paddingTop: 20,
    paddingBottom: 20
  },
  text3: {

  },
  phone: {
    borderColor: 'lightgray',
    borderWidth: 1,
    borderRadius: 5,
    height: 70,
    width: 310,
    padding: 1
  },
  button: {
    backgroundColor: '#0095C6',
    padding: 10,
    width: 160,
    borderRadius: 5,
    alignSelf: 'center',
    top: 45,
  },
  input: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  }
});
export default Login;