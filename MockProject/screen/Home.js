import React from "react";
import { StyleSheet, Text, View } from "react-native";


const Home = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text} >Welcome...</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    flex: 1
  },
  text: {
    fontSize: 50,
    fontWeight: 'bold'
  }
});
export default Home;