import React, { useRef, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

const Verify = ({ navigation }) => {

  const firstInput = useRef();
  const secondInput = useRef();
  const thirdInput = useRef();
  const fourthInput = useRef();
  const [otp, setOtp] = useState();

  return (
    <View style={styles.container}>
      <Text style={styles.icon} >
        <Icon name='angle-left' color={'black'} size={40} onPress={() => navigation.navigate('Login')} />
      </Text>
      <Text style={styles.text1} >OTP Verification</Text>
      <Text style={styles.text2}>Enter your OTP sent to your Mobile Number</Text>
      <View style={styles.otpContainer} >
        <View style={styles.otpBox}>
          <TextInput
            style={styles.otpText}
            keyboardType="number-pad"
            maxLength={1}
            ref={firstInput}
            onChangeText={text => {
              setOtp({ ...otp, 1: text });
              text && secondInput.current.focus();
            }}
          />
        </View>
        <View style={styles.otpBox}>
          <TextInput
            style={styles.otpText}
            keyboardType="number-pad"
            maxLength={1}
            ref={secondInput}
            onChangeText={text => {
              setOtp({ ...otp, 2: text });
              text ? thirdInput.current.focus() : firstInput.current.focus();
            }}
          />
        </View>
        <View style={styles.otpBox}>
          <TextInput
            style={styles.otpText}
            keyboardType="number-pad"
            maxLength={1}
            ref={thirdInput}
            onChangeText={text => {
              setOtp({ ...otp, 3: text });
              text ? fourthInput.current.focus() : secondInput.current.focus();
            }}
          />
        </View>
        <View style={styles.otpBox}>
          <TextInput
            style={styles.otpText}
            keyboardType="number-pad"
            maxLength={1}
            ref={fourthInput}
            onChangeText={text => {
              setOtp({ ...otp, 4: text });
              !text && thirdInput.current.focus();
            }}
          />
        </View>
      </View>
      <Text style={styles.text3}>Resend OTP</Text>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.input} onPress={() => navigation.navigate('Home')} >Verify</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    backgroundColor: 'white',
    flex: 1
  },
  image1: {
    height: 250,
    width: 250,
  },
  text1: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
    bottom: 32,
    left: 10
  },
  text2: {
    marginLeft: 10
  },
  text3: {
    color: 'skyblue',
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 10,
    marginTop: 25,
    fontWeight: 'bold'
  },

  button: {
    backgroundColor: '#0095C6',
    padding: 10,
    width: 150,
    height: 48,
    borderRadius: 5,
    alignSelf: 'center',
    top: 25,
  },
  input: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold'
  },
  icon: {
    right: 20
  },
  otpBox: {
    borderRadius: 50,
    borderColor: 'white',
    borderWidth: 1,
    width: 45,
    height: 45,
    backgroundColor: 'skyblue',
    margin: 5,
    marginLeft: 10

  },
  otpContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20
  },
  otpText: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold'
  }
});
export default Verify;